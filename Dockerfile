FROM alpine:3.10

# Labels about the Dockerfile 
LABEL name="EKS Toolset." \
    description="Kubectl, helm, kustomize and aws cli." \
    version="${HELM_VERSION}" \
    maintainer="Thibaut ALLAIN <thibaut.allain@orange.com>" \
    url="https://gitlab.com/farmtesting/images/eks-toolset"

ARG HELM_VERSION=v3.7.1
ARG KUBECTL_VERSION=v1.21.7
ARG ARGO_VERSION=v3.2.4

USER root

# install base packages
RUN apk add curl bash groff less python py-pip coreutils openssl jq

# install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl

# install helm
RUN curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar xz && \ 
    mv linux-amd64/helm /bin/helm && \ 
    rm -rf linux-amd64 && \
    curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash

# install AWS CLI
RUN pip install awscli

# install aws-iam-authenticator
RUN curl -LO https://amazon-eks.s3-us-west-2.amazonaws.com/1.11.5/2018-12-06/bin/linux/amd64/aws-iam-authenticator && \
    chmod +x ./aws-iam-authenticator && \
    mv ./aws-iam-authenticator /usr/local/bin/aws-iam-authenticator


# install Argo
RUN curl -sLO https://github.com/argoproj/argo/releases/download/${ARGO_VERSION}/argo-linux-amd64.gz && \
    gunzip argo-linux-amd64.gz && \
    chmod +x argo-linux-amd64 && \
    mv ./argo-linux-amd64 /usr/local/bin/argo

COPY scripts/docker-entrypoint.sh docker-entrypoint.sh

RUN chmod 755 docker-entrypoint.sh

ENTRYPOINT ["./docker-entrypoint.sh"]

CMD ["/bin/bash"]
